import hashlib
import logging
logger = logging.getLogger(__name__)

from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone
from django.views import generic

from .models import Choice, Question

PASSWORD_SALT = "43518e2530f6db29d2c80a65600b620cd4acfddc1b53e214063fef9bd6f3207d"

class IndexView(generic.ListView): #pyling: disable=C0111
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """
        Return the last five published questions (not including those set to be
        published in the future).
        """
        return Question.objects.filter(
            pub_date__lte=timezone.now()
        ).order_by('-pub_date')[:5]


class DetailView(generic.DetailView): #pyling: disable=C0111
    model = Question
    template_name = 'polls/detail.html'
    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Question.objects.filter(pub_date__lte=timezone.now())


class ResultsView(generic.DetailView): #pyling: disable=C0111
    model = Question
    template_name = 'polls/results.html'


def vote(request, question_id): #pyling: disable=C0111
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        messages.error(request, "You didn't select a choice.")
        return render(request, 'polls/detail.html', {
            'question': question
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))

class LoginPageView(generic.base.TemplateView): #pyling: disable=C0111
    template_name = 'polls/login.html'

def logins(request): #pyling: disable=C0111
    username = request.POST['username']
    password = request.POST['password']
    if username is not "" and password is not "":
        password_hashed = hashlib.sha512((PASSWORD_SALT + password).encode('utf-8')).hexdigest()
        user = authenticate(request, username=username, password=password_hashed)
        if user is not None:
            login(request, user)
            messages.success(request, "Login successful !")
            return HttpResponseRedirect(reverse('polls:index'))
        else:
            messages.error(request, "Bad credentials")
            return render(request, 'polls/login.html')
    else:
        messages.error(request, "Username or password unset.")
        return render(request, 'polls/login.html')

class RegisterPageView(generic.base.TemplateView): #pyling: disable=C0111
    template_name = 'polls/register.html'

def register(request): #pyling: disable=C0111
    username = request.POST['username']
    password = request.POST['password']
    cpassword = request.POST['cpassword']
    if username is not "" and password is not "" and cpassword is not "":
        if password == cpassword:
            if User.objects.filter(username=username).exists() is False:
                if len(password) > 5:
                    password_hashed = hashlib.sha512((PASSWORD_SALT + password).encode('utf-8')).hexdigest()
                    User.objects.create_user(username=username, password=password_hashed)
                    messages.success(request, "Register successful.")
                    return HttpResponseRedirect(reverse('polls:login_page'))
                else:
                    messages.error(request, "Your password is too short (it shoulds be greater than 5 characters).")
                    return render(request, 'polls/register.html')
            else:
                messages.error(request, "Username already exists.")
                return render(request, 'polls/register.html')
        else:
            messages.error(request, "Passwords don't match.")
            return render(request, 'polls/register.html')
    else:
        messages.error(request, "Required fields unsets.")
        return render(request, 'polls/register.html')

def logouts(request):
    logout(request)
    messages.success(request, "Logged out.")
    return HttpResponseRedirect(reverse('polls:index'))