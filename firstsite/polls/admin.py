from django.contrib import admin
from .models import Question, Choice
# Register your models here.

class ChoiceInline(admin.TabularInline): #pyling: disable=C0111
    model = Choice
    extra = 3

class QuestionAdmin(admin.ModelAdmin): #pyling: disable=C0111
    fieldsets = [
        (None,               {'fields': ['question_text']}), #pylint: disable=C0326
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]
    list_display = ('question_text', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question_text']

admin.site.register(Question, QuestionAdmin)
