from django.contrib import admin
from wishlist.models import Wish, WishList

# Register your models here.
class WishInline(admin.TabularInline): #pyling: disable=C0111
    model = Wish
    extra = 1

class WishListAdmin(admin.ModelAdmin): #pyling: disable=C0111
    fieldsets = [
        (None,               {'fields': ['name']}), #pylint: disable=C0326
        ('Date information', {'fields': ['created_date'], 'classes': ['collapse']}),
    ]
    inlines = [WishInline]
    list_display = ('name', 'created_date', 'owner')
    list_filter = ['created_date', 'owner']
    search_fields = ['name']

admin.site.register(WishList, WishListAdmin)
