from django.urls import path, include

from wishlist.views import IndexView, RegisterView, RegisterView, CreateListView, ReadWishListView, WishListListView, WishListEditView

app_name = 'wishlist'
urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/register', RegisterView.as_view(), name='register'),
    path('wishlist/create', CreateListView.as_view(), name='wishlist_create'),
    path('wishlist/<int:pk>', ReadWishListView.as_view(), name='wishlist_read'),
    path('wishlist/', WishListListView.as_view(), name='wishlist_list'),
    path('wishlist/<int:pk>/edit', WishListEditView.as_view(), name='wishlist_edit')
]
