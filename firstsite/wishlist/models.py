from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class WishList(models.Model):
    name = models.CharField(max_length=200)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    created_date = models.DateTimeField('date of creation')

class Wish(models.Model):
    name = models.CharField(max_length=200)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    description = models.CharField(max_length=200,blank=True)
    wishlist = models.ForeignKey(WishList, on_delete=models.CASCADE)

