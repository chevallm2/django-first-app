from django.shortcuts import render
from django import views
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from django.urls import reverse
from django.forms import formset_factory
from django.db import transaction, IntegrityError
from wishlist.messages import ErrorMessage

from wishlist.models import WishList, Wish
from wishlist.forms import WishListForm, WishForm

import logging
logger = logging.getLogger(__name__)

# Create your views here.
class IndexView(views.generic.base.TemplateView): #pylint: disable=C0111
    template_name = 'wishlist/index.html'

class RegisterView(views.View):
    def post(self, request):
        # Retrieve the form with POST data
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            # Create a user
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('index')
        else:
            messages.error(request, ErrorMessage.INVALID_FORM)
            form = UserCreationForm()
        return render(request, 'registration/register.html', {'form': form}) 

    def get(self, request):
        form = UserCreationForm()
        return render(request, 'registration/register.html', {'form': form})

class CreateListView(views.generic.CreateView):
    model = WishList
    def get(self, request):
        # Create a empty Wish list creation form
        form = WishListForm()
        return render(request, 'wishlist/wishlist_create.html', {'form': form})

    def post(self, request):
        # Retrieve the form filled with POST data
        form = WishListForm(request.POST)
        # If there is a logged user
        if request.user.is_authenticated:
            # And the form is valid
            if form.is_valid():
                # Create a Wishlist
                name = form.cleaned_data.get('name')
                wishlist = WishList(name=name, created_date=timezone.now(), owner=request.user)
                logger.info("Saving a wishlist to db.")
                # Save it in DB
                wishlist.save()
                # Redirect to consulting view
                return redirect(reverse('wishlist:wishlist_read', kwargs={'pk': wishlist.id}))
            else:
                messages.error(request, ErrorMessage.INVALID_FORM)
                return render(request, 'wishlist/wishlist_create.html', {'form': form})   
        else:
            messages.error(request, ErrorMessage.LOGGIN_REQUIRED)
            return render(request, 'registration/login.html')

class ReadWishListView(views.generic.DetailView):
    model = WishList
    template_name = 'wishlist/wishlist_read.html'

class WishListListView(views.generic.ListView):
    template_name = 'wishlist/wishlist_list.html'
    context_object_name = 'wishlists'
    # Returns wishlist created by the current user and user data
    def get_queryset(self):
        return {
            'wishlists': WishList.objects.filter(
                owner=self.request.user
            )
        }

class WishListEditView(views.View):
    def get(self, request, pk):
        # Get the Wishlist
        wishlist = WishList.objects.get(pk=pk)
        # Load wishlist data in form
        wishlist_form = WishListForm(instance=wishlist)
        # Get wish that are in the wishlist
        wishs = Wish.objects.filter(wishlist=wishlist).values()
        # Create a form for each wish and an extra one too add a wish.
        WishFormSet = formset_factory(WishForm, extra=1)
        # Load wish data in the form set
        wish_formset = WishFormSet(initial=wishs)
        # Render all.
        return render(request, 'wishlist/wishlist_edit.html', {'wishlist_form': wishlist_form, 'wish_formset': wish_formset, 'wishlist': wishlist})

    def post(self, request, pk):
        # Get the Wishlist
        wishlist = WishList.objects.get(pk=pk)
        # Retrieve the wishlist form with POST data
        wishlist_form = WishListForm(request.POST)
        # Instanciate a Wish form set
        WishFormSet = formset_factory(WishForm)
        # Retrieve the wish forms with POST data
        wish_formset = WishFormSet(request.POST)

        # If both wishlist form and wish forms are valid
        if wishlist_form.is_valid() and wish_formset.is_valid():

            # Update the wishlist data
            wishlist.name = wishlist_form.cleaned_data.get('name')
            wishlist.save()

            new_wish = []
            # Create a Wish object for each wish form
            for wish_form in wish_formset:
                name = wish_form.cleaned_data.get('name')
                price = wish_form.cleaned_data.get('price')
                description = "" if wish_form.cleaned_data.get('description') is None else wish_form.cleaned_data.get('description')
                
                if name and price and description:
                    new_wish.append(Wish(name=name, price=price, description=description))
                try:
                    with transaction.atomic():
                        # Replace olds wish with news
                        Wish.objects.filter(wishlist=wishlist).delete()
                        Wish.objects.bulk_create(new_wish)

                        # Add a notification
                        messages.success(request, "Wishlist updated.")
                except IntegrityError as exception:
                    # In case of error
                    # Add a failure notification
                    messages.error(request, ErrorMessage.ERROR_UPDATING_CHANGE)
                    logger.error(exception)
                    return redirect(reverse('wishlist:wishlist_edit', args=[pk]))
        else:
            messages.error(request, ErrorMessage.INVALID_FORM)

        context = {
            'wishlist_form': wishlist_form,
            'wish_formset': wish_formset
        }

        return render(request, 'wishlist/wishlist_edit.html', context)
        