from django import forms
from wishlist.models import WishList, Wish


class WishListForm(forms.ModelForm):
    """
    Form that let you manipulate a WishList model
    """
    class Meta:
        model = WishList
        fields = ['name']

class WishForm(forms.ModelForm):
    class Meta:
        model = Wish
        fields = ['name', 'price', 'description']